from logging.handlers import RotatingFileHandler
from start import application

if __name__ == '__main__':
    handler = RotatingFileHandler('pts.log', maxBytes=10000, backupCount=1)
    handler.setLevel(logging.INFO)
    application.logger.addHandler(handler)
    application.run()
