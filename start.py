from flask import Flask,render_template,jsonify,json,request
import traceback
import logging

from db.models import connect, get_machines_with_max_rms,\
    get_rms_trend
import json
import sys

sys.path.append('/app')
sys.path.append('/app/db')
application = Flask(__name__)


@application.route('/getMachine/<mmt_loc_id>', methods=['POST', 'GET'])
def get_trend(mmt_loc_id = 0):
    try:
        session, engine = connect()
        rms_trend = json.dumps(get_rms_trend(session, mmt_loc_id, 'csv'))
        return rms_trend
    except:
        tb = traceback.format_exc()
        application.logger.error(tb)
        return render_template('404.html')
    finally:
        if session:
            session.close()

@application.route('/')
def showMachineList():
    return render_template('lst.html')

@application.route("/getMachineList",methods=['GET'])
def getMachineList():
    try:
        session, engine = connect()
        machineList = get_machines_with_max_rms(session)
    except:
        tb = traceback.format_exc()
        application.logger.error(tb)
        return render_template('404.html')
    finally:
        if sesssion:
            session.close()
    return json.dumps(machineList)
