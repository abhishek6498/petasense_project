import unittest
from db.models import connect, MachineDetails

class TestModels(unittest.TestCase):
    def test_machine_details(self):
        session, engine = connect()
        session.add(
            MachineDetails(machine = 'Test#1',
                description = "Test#1",
                state = 'ON'
            )
        )
        session.commit()
        machines = MachineDetails.get_machines(session)
        for machine in machines:
            if machine.machine == 'Test#1':
                self.assertEqual(machine.machine, 'Test#1')


    def test_connection(self):
        session, engine = connect()
        self.assertNotNull(session)
        session.close()

if __name__ == '__main__':
    unittest.main()
