import random
from models import MachineDetails,\
    MeasurementLocation, Vibration, get_machines_with_max_rms,\
    get_rms_trend, connect
from datetime import datetime, timedelta

def insert_sample_machines(session):
    session.add(
        MachineDetails(machine = 'El Dorado Pump 1',
             description = ("Pumps chopped tomatoes through hot "
             "break deactivating enzymes for thicker consistency"),
             state = 'ON'
         )
    )
    session.add(
        MachineDetails(machine = 'AH 15050 Air Handler (EIN 71630)',
             description = ("Provide fan curves with specified operating "
             "point clearly plotted"),
             state = 'NA'
         )
    )

    session.add(
        MachineDetails(machine = 'El Dorado Pump 2',
             description = ("Reliable Wastewater Pumps - Effective "
             "automated aeration Self-adjusting - Easy to maintain "),
             state = 'OFF'
         )
    )

    session.add(
        MachineDetails(machine = 'Feed Pump 1',
             description = ("Pumps chopped tomatoes through hot "
             "break deactivating enzymes for thicker consistency"),
             state = 'ON'
         )
    )
    session.commit()


def insert_sample_measurement_locations(session, machine_list):
    if not machine_list or len(machine_list) < 4:
        return
    session.add(
        MeasurementLocation(name = 'L1',
            machine_id = machine_list[0].id
        )
    )
    session.add(
        MeasurementLocation(name = 'L1',
            machine_id = machine_list[1].id
        )
    )
    session.add(
        MeasurementLocation(name = 'L2',
            machine_id = machine_list[0].id
        )
    )
    session.add(
        MeasurementLocation(name = 'L2',
            machine_id = machine_list[1].id
        )
    )

    session.add(
        MeasurementLocation(name = 'L2',
            machine_id = machine_list[2].id
        )
    )

    session.add(
        MeasurementLocation(name = 'L2',
            machine_id = machine_list[3].id
        )
    )

    session.add(
        MeasurementLocation(name = 'L3',
            machine_id = machine_list[0].id
        )
    )

    session.add(
        MeasurementLocation(name = 'L4',
            machine_id = machine_list[0].id
        )
    )

    session.add(
        MeasurementLocation(name = 'L5',
            machine_id = machine_list[0].id
        )
    )
    session.commit()


def insert_sample_vibration_data(session, mmt_locs):
    if mmt_locs is None:
        return
    for location in mmt_locs:
        # random vibration for 3600 hrs = 5 months * 30 days * 24 hrs
        num_hrs = 5 * 30 * 24
        time_now = datetime.now()
        while num_hrs >= 0:
            time_now = time_now - timedelta(hours = 1)
            session.add(
                Vibration(location = location.id,
                    measurement_time = time_now,
                    vbrn_val = random.uniform(0.25, 1.5)
                )
            )
            num_hrs -= 1
    session.commit()


if __name__ == '__main__':
    session, engine = connect()
    insert_sample_machines(session)
    machines = MachineDetails.get_machines(session)
    insert_sample_measurement_locations(session, machines)
    mmt_locs = MeasurementLocation.get_mmt_locs(session)
    insert_sample_vibration_data(session, mmt_locs)
