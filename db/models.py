import datetime
import os
import json

from sqlalchemy import func, and_
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, ForeignKey, UniqueConstraint, Column, Integer,\
      Enum, Float, Text, String, DateTime, event
from sqlalchemy_utils import database_exists, create_database
from sqlalchemy.orm import sessionmaker


Base = declarative_base()

# Model of a machine
class MachineDetails(Base):
    __tablename__    = 'machine_details'

    id               = Column(Integer, primary_key = True, autoincrement = True)
    machine          = Column(String(200), nullable = False)
    description      = Column(Text)
    state            = Column(Enum('ON', 'OFF', 'NA'))
    state_updated_at = Column(DateTime, default=datetime.datetime.utcnow, nullable = False)

    def __repr__(self):
        return "%s, %s, %s" % (self.machine, self.description, self.state)

    @classmethod
    def get_machines(cls, session):
        return session.query(cls).all()




@event.listens_for(MachineDetails.state, 'set')
def state_updated_at_set(target, value, old_value, initiator):
    """
        Event listener on state
        When the state is changed, state_updated_at
        datetime should also be updated.
    """
    if value != old_value:
        target.state_updated_at = datetime.datetime.now()






# Measurement location depends on which machine it is there
class MeasurementLocation(Base):
    __tablename__    = 'measurement_location'
    id               = Column(Integer, primary_key = True, autoincrement = True)
    name             = Column(String(10), nullable = False)
    machine_id       = Column(
                         Integer,
                         ForeignKey('machine_details.id', ondelete="CASCADE"),
                         nullable = False
                       )
    __table_args__   = (UniqueConstraint('name', 'machine_id'),)

    @classmethod
    def get_mmt_locs(cls, session):
        return session.query(cls).all()


# Vibrations is generated from a measurement location
class Vibration(Base):
    __tablename__    = 'vibration'
    id               = Column(
                           Integer,
                           primary_key = True,
                           autoincrement = True
                       )
    location         = Column(
                           Integer,
                           ForeignKey('measurement_location.id', ondelete="CASCADE"),
                           nullable = False
                       )
    measurement_time = Column(DateTime, nullable = False)
    vbrn_val         = Column(Float)

    @classmethod
    def get_vibrations(cls, session):
        return session.query(cls).all()

# Module 2 API 1
def get_machines_with_max_rms(session):
    results = (session.query(MachineDetails, func.max(Vibration.vbrn_val))
        .join(MeasurementLocation, and_(MachineDetails.id == MeasurementLocation.machine_id))
        .join(Vibration, and_(MeasurementLocation.id == Vibration.location))
        .group_by(MachineDetails.id)
        ).all()
    res = []
    for item in results:
        res.append({'id': item[0].id, 'machine': item[0].machine,
            'desc': item[0].description,
            'state': item[0].state, 'rms': item[1]})
    return res

# Module 2 API 2
def get_rms_trend(session, mmt_loc_id, output = 'csv'):
    vibrations = Vibration.get_vibrations(session)
    rms_trend = []
    for vibration in vibrations:
        res = {}
        if int(vibration.location) != int(mmt_loc_id):
            continue
        res['date'] = str(vibration.measurement_time)
        res['price']= vibration.vbrn_val
        rms_trend.append(res)
    return rms_trend



# connect to models
def connect():
    with open(os.path.join(os.path.dirname(__file__), 'db.cfg')) as _:
        config = json.load(_)
    engine = create_engine(config.get('conn_str') % config)
    session = sessionmaker(bind=engine)
    return (session(), engine)


# Sets the table and databases
def setUp():
    session, engine = connect()
    if not database_exists(engine.url):
        create_database(engine.url)
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)


if __name__ == '__main__':
    setUp()
